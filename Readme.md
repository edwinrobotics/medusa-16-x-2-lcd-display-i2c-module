# Medusa 16 x 2 LCD Display I2C Module

![Medusa 16 x 2 LCD Display with I2C Module](https://shop.edwinrobotics.com/4061-thickbox_default/medusa-16-x-2-lcd-display.jpg "Medusa 16 x 2 LCD Display with I2C Module")

[Medusa 16 x 2 LCD Display](https://shop.edwinrobotics.com/medusa/1213-medusa-16-x-2-lcd-display.html)

This is a serial I2C adapter board pre-soldered to the back of the LCD. It can be controlled with just 2 data pins (SDA & SCL) instead of the usual 6 lines used to control LCD, Contrast adjustment and backlight disable jumper is also provided in this board.These modules are currently supplied with a default I2C address of 0x3F and address is configurable via onboard jumper pads.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.